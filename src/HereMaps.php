<?php

namespace Helium\HereMaps;

class HereMaps
{
	public static $GEN = 9;

	public static function reverseGeocode($lat, $long, $radius = 250, $max = 1) {
		$app_key = env('HERE_MAPS_APP_KEY');
		$gen = self::$GEN;

		$url = "https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long,$radius&"
			. "mode=retrieveAddresses&maxresults=$max&gen=$gen&apiKey=$app_key";

		try {
			$response = file_get_contents($url);
			return json_decode($response)->Response->View[0];
		}
		catch (\Exception $e){
			return false;
		}
	}

	public static function reverseGeocodeArea($lat, $long, $radius = 250, $max = 1)
    {
        $app_key = env('HERE_MAPS_APP_KEY');
        $gen = self::$GEN;

        $url = "https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long,$radius&"
            . "mode=retrieveAreas&maxresults=$max&gen=$gen&apiKey=$app_key";

        try {
            $response = file_get_contents($url);
            return json_decode($response)->Response->View[0];
        }
        catch (\Exception $e){
            return false;
        }
    }

	public static function getTimezone($lat, $long, $radius = 250, $max = 1) {

		$app_key = env('HERE_MAPS_APP_KEY');

		$url = "https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=$lat,$long,$radius&"
			. "apiKey=$app_key&locationattributes=timeZone&mode=retrieveAddresses&maxresults=$max&gen="
			. self::$GEN;

		try {
			$response = file_get_contents($url);
			return json_decode($response)->Response->View[0]->Result[0]->Location->AdminInfo->TimeZone;
		}
		catch (\Exception $e){
			return false;
		}

	}

	public static function getLocation($address, $address2, $city, $state, $zip) {

		$app_key = env('HERE_MAPS_APP_KEY');

		$search = '';

		if (!empty($address)) {
			$search .= $address . ' ';
		}
		if (!empty($address2)) {
			$search .= $address2 . ' ';
		}
		if (!empty($city)) {
			$search .= $city . ' ';
		}
		if (!empty($state)) {
			$search .= $state . ' ';
		}
		if (!empty($zip)) {
			$search .= $zip . ' ';
		}

		$search = rawurlencode($search);

		$url = "https://geocoder.ls.hereapi.com/search/6.2/geocode.json?searchtext=$search&apiKey=$app_key&gen="
			. self::$GEN;

		try {
			$response = file_get_contents($url);
			return json_decode($response)->Response->View[0]->Result;
		}
		catch (\Exception $e){
			return false;
		}
	}
}